Android 程序设计课程项目
===================
2020 秋季学期 教师 简星


第三周 布局管理器 RelativeLayout
--------------------------------

#### 作业题目

请使用 RelativeLayout 布局管理器按下图完成一个登录界面的设计<br>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0919/130253_e81a49e6_7835109.png "relativelayout1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0919/130305_aa7b6dcf_7835109.png "relativelayout2.png")

#### 作业说明
请使用：相对布局RelativeLayout，文本TextView，输入框EditText，按钮Button等组件完成以上作业。<br>
界面元素位置在任何比例的屏幕下都保持基本不变。

#### 作业要求：
使用 Eclipse 工具；独立完成；包名前缀为 edu.cque.yourname，

#### 完成时间：
一周